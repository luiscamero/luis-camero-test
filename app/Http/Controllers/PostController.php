<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Ixudra\Curl\Facades\Curl;

class PostController extends Controller
{
    public function index (Request $request) {
        $response = Curl::to('https://sq1-api-test.herokuapp.com/posts')
        ->asJson()
        ->get();
        Post::where('author_id', 1)->delete();
        foreach ($response->data as $key => $value) {
            $post = new Post();

            $post->title = $value->title;
            $post->description = $value->description;
            $post->created_at = $value->publication_date;
            $post->author_id = 1;
            
            $post->save();
        }
        $order = $request->input('order', 'desc');
        $posts = Post::orderBy('created_at', $order)->get();
        return view('welcome', ['posts' => $posts, 'order' => $order]);
    }

    public function create () {
        return view('user.create');
    }

    public function store (Request $request) {
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required|min:10'
        ]);

        // return $validated;

        $post = new Post();
        $post->title = $validated['title'];
        $post->description = $validated['description'];
        $post->author_id = Auth::id();
        $post->save();

        return redirect('home')->with('status', 'Post created!');
    }
}
