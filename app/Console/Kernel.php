<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Ixudra\Curl\Facades\Curl;
use App\Models\Post;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            $response = Curl::to('https://sq1-api-test.herokuapp.com/posts')
            ->asJson()
            ->get();
            Post::where('author_id', 1)->delete();
            foreach ($response->data as $key => $value) {
                $post = new Post();

                $post->title = $value->title;
                $post->description = $value->description;
                $post->created_at = $value->publication_date;
                $post->author_id = 1;
                    
                $post->save();
            }
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
