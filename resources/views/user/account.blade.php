@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row my-3 justify-content-between">
            <div class="col-4">
                <h3>
                    My account
                </h3>
            </div>
            <div class="col-2">
                <a href="{{ route('create') }}" class="btn btn-primary float-end">
                    Create post
                </a>
            </div>
        </div>
        <div class="row">
            @foreach ($posts as $post)
                <div class="col-md-4 mt-2">
                    <div class="card">
                        <div class="card-header">{{ $post->title }}</div>

                        <div class="card-body">
                            <p>
                                {{ $post->description }}
                            </p>

                        </div>

                        <div class="card-footer">
                            <span class="float-end">
                                {{ $post->created_at }}
                            </span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
