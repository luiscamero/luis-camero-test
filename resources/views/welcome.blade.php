@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-3 justify-content-between">
            <div class="col-4">
                <h3>
                    All posts
                </h3>
            </div>
            <div class="col-2">
                <form action="{{ Route('welcome') }}" method="get">
                    <div class="form-group row mb-2">
                        <label for="order" class="col-md-4 col-form-label text-md-right">Sort</label>

                        <div>
                            <select value="{{ $order }}" onchange="this.form.submit()" name="order" id="order"
                                class="form-control">
                                <option @if($order == 'desc') selected @endif value="desc">Newer to older</option>
                                <option @if($order == 'asc') selected @endif value="asc">Older to newer</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            @foreach ($posts as $post)
                <div class="col-md-4 mt-2">
                    <div class="card">
                        <div class="card-header">{{ $post->title }}</div>

                        <div class="card-body">
                            <p>
                                {{ $post->description }}
                            </p>

                        </div>

                        <div class="card-footer">
                            <span class="float-end">
                                {{ $post->created_at->format('M-d-Y H:i') }}
                            </span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
