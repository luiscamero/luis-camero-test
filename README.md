# Luis Camero Test

This is my answer to the assignment [here](https://www.notion.so/Web-Developer-0cdf0bb1015d4e5c94b62b3fe61ee621)
## Installation
After cloning the repository, create a database called blog (or change the name of the database in the .env file). Once the database is created, run the migrations with the seeder. 

```bash
composer install
```

```bash
php artisan migrate --seed
```

## Usage

The seeder will create the admin user which will be the author of the imported posts. Then run the project:

```bash
php artisan serve
```

Once the project is running, you can will see an empty page of posts, but you can register and login. Once you login, there's a dropdown menu where you can either logout or go to your account, where you'll see the posts that you have created and will be able to create new ones. 

## Importing posts
The project is setup to request the given endpoint every hour (at 5 o'clock, 6 o'clock and so) and save the post under the admin user. To test this locally you could use the following command:

```bash
php artisan schedule:work
```